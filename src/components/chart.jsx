import React from 'react';
import { PieChart, Pie, Tooltip } from 'recharts';



export default class Chart extends React.Component {

   
    render() {
        return (
            <PieChart width={500} height={500}>
                <Pie 
                dataKey="value" 
                isAnimationActive={true} 
                data={[
                    { name: "🌚", value: this.props.xPoints }, 
                    { name: '☔', value: this.props.oPoints }
                ]} 
                cx={200} 
                cy={120} 
                outerRadius={110} 
                fill="#007a72" 
                 />

                <Tooltip />
            </PieChart>
        );
    }
}