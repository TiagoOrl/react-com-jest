import React, { Component } from "react";

class Timer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      totalSeconds: 0,
      seconds: 0,
    };
  }

  tick() {
    const { onFinish } = this.props;

    if (onFinish === true) {
      this.setState({
        seconds: 0 // caso o jogo tenha acabado: taxa de variacao de segundos é zero => contador não muda e para no tempo contado até o instante
      });
      
    } else {
      this.setState(state => ({
        seconds: state.seconds + 1,
        totalSeconds: this.state.seconds
      }));
      
    }
  }

  
  componentDidMount() {
    this.interval = setInterval(() => this.tick(), 1000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  render() {
    return <div>Segundos: {this.state.totalSeconds}</div>;
  }
}

export default Timer;
