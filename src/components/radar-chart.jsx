import React from 'react';
import {RadarChart, PolarGrid, PolarRadiusAxis, PolarAngleAxis, Radar, Legend} from 'recharts';


let dados = [
    {
        "subject" : 0,
        "A": this.props.squares[0] 
    },
    {
        "subject" : 1,
        "A": this.props.squares[1] 
    },
    {
        "subject" : 2,
        "A": this.props.squares[2]
    },
    {
        "subject" : 3,
        "A": this.props.squares[3] 
    },
    {
        "subject" : 4,
        "A": this.props.squares[4] 
    },
    {
        "subject" : 5,
        "A": this.props.squares[5] 
    },
    {
        "subject" : 6,
        "A": this.props.squares[6] 
    },
    {
        "subject" : 7,
        "A": this.props.squares[7] 
    },
    {
        "subject" : 8,
        "A": this.props.squares[8] 
    }
    
]

export default class RChart extends React.Component {


    componentDidMount() {
        console.log("componente montou...")
    }


    render() {
        return (
            <RadarChart outerRadius={100} width={300} height={250} data={}>
                <PolarGrid />
                <PolarAngleAxis dataKey="subject" />
                <PolarRadiusAxis angle={90} domain={[0, 5]} />

                <Radar name='Posições' dataKey='A' stroke='#A21810' fill="#2284d8" fillOpacity={0.5} />
            <Legend />
            </RadarChart>
        )
    };
}