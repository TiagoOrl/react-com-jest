import React, { Component } from "react";

import Board from "./board";
import Timer from "./timer";
import Chart from "./chart"
import RChart from './radar-chart';

import axios from "axios";

class Game extends Component {

  constructor(props) {
    
    super(props);


    this.xPoints = 1
    this.oPoints = 1 

    this.state = {
      history: [
        {
          squares: Array(9).fill(null)
        }
      ],
      stepNumber: 0,
      xIsNext: true,

      carregando: false,
      erros: "",
      dados: [],

    };
  }

  // http://www.mocky.io/v2/5ce554782e0000ca81f83d04 err: 500
  // http://www.mocky.io/v2/5ce2e2ba340000147b7737ae?mocky-delay=3000ms ok: 400
  // http://www.mocky.io/v2/5ce560842e00005000f83d85 bule de cha: 418
  // http://www.mocky.io/v2/5ce583ff2e0000289bf83e63 proibido: 403


  async handleFetchData() {
    try {

      this.setState({
        carregando: true
      });
      
      const res = await axios.get(`http://www.mocky.io/v2/5ce560842e00005000f83d85?mocky-delay=2000ms`);
      console.log(res);

      this.setState({
        dados: res.data,
        carregando: false
      });

      return res.data;

    } catch (err) {

      if (err.response.status === 500){
        this.setState({
          erros: "500: erro interno de servidor!!!",
          carregando: false
        })
      }

      if (err.response.status === 502){
        this.setState({
          erros: "502: Bad Gateway!!!",
          carregando: false
        })
      }

      if (err.response.status === 409){
        this.setState({
          erros: "409: Conflito!!!",
          carregando: false
        })
      }

      if (err.response.status === 403){
        this.setState({
          erros: "403: Acesso proibido!!!",
          carregando: false
        })
      }

      if (err.response.status === 418){
        this.setState({
          erros: "418: Eu sou um bule de chá :)",
          carregando: false
        })
      }
    }
  }

  addPontos(winner) {
    winner.player === '🌚' ? this.xPoints += 1 : this.oPoints += 1
  }

  handleClick(i) {
    const locations = [
      [1],
      [2],
      [3],
      [4],
      [5],
      [6],
      [7],
      [8],
      [9]
    ];

    const history = this.state.history.slice(0, this.state.stepNumber + 1);
    const current = history[history.length - 1];
    const squares = current.squares.slice();

    if (this.calculateWinner(squares) || squares[i]) {
      return;
    }
    squares[i] = this.state.xIsNext ? "🌚" : "☔";

    this.setState({
      history: history.concat([
        {
          squares: squares,
          location: locations[i] // pelo parametro 'i', é possivel saber a posição do quadrado, que está na mesma ordem de 'locations'
        }
      ]),
      stepNumber: history.length,
      xIsNext: !this.state.xIsNext
    });
  }



  jumpTo(step) {
    this.setState({
      stepNumber: step,
      xIsNext: step % 2 === 0
    });
  }

  calculateWinner(squares) {
    const lines = [
      [0, 1, 2],
      [3, 4, 5],
      [6, 7, 8],
      [0, 3, 6],
      [1, 4, 7],
      [2, 5, 8],
      [0, 4, 8],
      [2, 4, 6]
    ];
    for (let i = 0; i < lines.length; i++) {
      const [a, b, c] = lines[i];
      if (
        squares[a] &&
        squares[a] === squares[b] &&
        squares[a] === squares[c]
      ) {
        return { player: squares[a], line: [a, b, c] };
      }
    }
    return null;
  }


  render() {
    const history = this.state.history;
    const current = history[this.state.stepNumber];
    let winner = this.calculateWinner(current.squares);

    const moves = history.map((step, move) => {
      const desc = move
        ? "Go to move #" + move + " @ " + history[move].location
        : "Go to game start";

      return (
        <li key={move}>
          <button className="btn btn-warning mb-1" onClick={() => this.jumpTo(move)}>{desc}</button>
        </li>
      );
    });

    let status;

    let finishGame = false;

    if (winner) {
      status = `Winner: ${winner.player} @ ${winner.line}`;
      finishGame = true;

      this.addPontos(winner)
      winner = null


    } else if (!current.squares.includes(null)) {
      finishGame = true;
      status = "draw";

    } else {
      finishGame = false;
      const nextPlayer = this.state.xIsNext ? "🌚" : "☔";

      status = "Next player: " + nextPlayer;
    }

    const { carregando } = this.state;

    return (

      <div className="game">
        <div className="game-board">
          <Timer onFinish={finishGame} />

          <div className="mt-1 mb-1">{status}</div>

          <Board
            winningSquares={winner ? winner.line : []}
            squares={current.squares}
            onClick={i => this.handleClick(i)}
          />
        </div>

        <div className="game-info">

          <div>
            < RChart squares={current.squares }/>
          </div>

          <ol>{moves}</ol>
        </div>
       
        <div>
          <span role="img" aria-label="emoji">☔ Points: {this.xPoints}<p></p></span>
          <span role="img" aria-label="emoji">🌚 Points: {this.oPoints}</span>
          < Chart xPoints={this.xPoints} oPoints={this.oPoints} />
        </div>
        
        <div>
          <button
              className="btn btn-primary mr-2"
              disabled={true}
              hidden={!carregando}
            >
              {carregando && (
                <span className="spinner-border spinner-border-sm text-warning" />
              )}
            </button>

            <button
              className="btn btn-primary"
              onClick={() => this.handleFetchData()}
              disabled={carregando}
            >
              Obter Dados
            </button>
            
            <button 
            className="btn btn-danger m-1" 
            hidden={ !this.state.erros ? true : false } 
            disabled={true}>

              {this.state.erros}
            </button>

          <ul>
            <b>Cores:</b>
            {this.state.dados.map(dado => (
              <li key={dado.peso}>{dado.cor}</li>
            ))}
          </ul>
          
          <ul>
            <b>Pesos:</b>
            {this.state.dados.map(dado => (
              <li key={dado.peso}>{dado.peso}</li>
            ))}
          </ul>
        </div>

      </div>
      
      
    );
  }
}

export default Game;
