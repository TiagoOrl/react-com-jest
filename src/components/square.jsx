import React from 'react';
import axios from 'axios';


function Square(props) {
    // console.log('props', props)
      return (
          <button 
              className={"square " + (props.isWinning ? "square--winning" : null)} 
              onClick={props.onClick} /* event raiser */ 
          > 
              {props.value}    
          </button>
      );
  }
  

  export default Square;