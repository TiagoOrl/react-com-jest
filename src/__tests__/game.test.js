

import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Game from '../components/game';


describe('SUITE: Testando o Game Component', () => {


    fit(' Deve renderizar apenas o Game component corretamente', () => {
      const wrapper = shallow(
        <Game />
      );
      expect(wrapper).toMatchSnapshot();
    });

    fit(' Deve conter um node Board que exista', () => {
      const wrapper = shallow(
        <Game />
      );

      expect(wrapper.find('Board').exists()).toBe(true);
    })


    fit(" Deve conter um node Timer que exista", () => {
      const wrapper = mount(
        <Game />
      );

      expect(wrapper.find('Timer').exists()).toBe(true);
    })

    wrapper.unmount();

});