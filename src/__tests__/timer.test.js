import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Timer from '../components/timer';
import { async } from 'q';
import { tsExternalModuleReference, exportAllDeclaration, jsxEmptyExpression } from '@babel/types';
import { setTimeout } from 'timers';

jest.useFakeTimers();

describe("SUITE: testando o componente Timer", () => {

    

    fit("Timer deve retornar 3 segundos: ", async () => {
        const wrapper = mount(<Timer onFinish={false}/>);
        
        console.log(wrapper.state('totalSeconds'))

        
        setTimeout(() => wrapper.setProps({onFinish: true}), 3000)
        


        console.log(wrapper.prop('onFinish'))

        expect(wrapper.state('totalSeconds')).toBe(3)
        

        
        wrapper.unmount();
    })
})