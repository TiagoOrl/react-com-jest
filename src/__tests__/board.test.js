
import React from 'react';
import { shallow, mount, render } from 'enzyme';
import Board from '../components/board';



describe("SUITE: Testando o Board Component", () => {

    it("<Board> Deve conter um node Square que exista.", () => {

        const wrapper = mount(< Board winningSquares={[]} squares={['oi', 'ola']} />);    //props que o componente passa para o Square: não dar erro de undefined

        expect(wrapper.prop('squares')).toEqual(['oi', 'ola']);
        wrapper.unmount();
    })

    it ('<Board> Deve conter um node Square com um X', () => {
        const wrapper = mount (
          <Board squares={Array(9).fill("X", 0, 1)} winningSquares={[]}/>  // como se a classe pai: Game estivesse passando props para a classe filha Board
        );
  
        console.log(wrapper.props());
        console.log(wrapper.prop("squares")); // é possível ver que o wrapper contém a prop squares[] com um valor X
        
        expect(wrapper.prop('squares')).toContain("X");
        wrapper.unmount();
      
      });
})