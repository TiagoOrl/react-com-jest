
import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Square from '../components/square';
import Board from '../components/board';
import Chart from '../components/chart';

storiesOf('Square', module)
    .add('com letras', () => (
        <Square value='C'/>
    ))
    .add('com emoji', () => (
        <Square value={<span role="img" aria-label="so cool">🌚</span>} />
    ));

storiesOf('Board', module)
    .add('com emoji', () => (
        <Board squares={Array(9).fill("🏃", 0, 3)} winningSquares={[2,4,6]} />
    ));


storiesOf('Chart', module)
    .add('4 e 5', () => (
        < Chart xPoints={4} oPoints={5} />
    ));
